import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { LanguageModule } from './language/language.module';
import { BugsModule } from './bugs/bugs.module';
import { PhotoModule } from './photo/photo.module';
import { EmailModule } from './email/email.module';
import MYSQL_CONFIG from './shared/config/mysql.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AuthModule,
    UsersModule,
    TypeOrmModule.forRootAsync({
      useFactory: () => MYSQL_CONFIG,
    }),
    LanguageModule,
    BugsModule,
    PhotoModule,
    EmailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
