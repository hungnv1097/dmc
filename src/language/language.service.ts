import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Language } from './entity/language.entity';
import { parseLanguageWithParams } from 'src/shared/utilities/language.util';
import { LANGUAGE_ID } from 'src/shared/constants/system.constant';

@Injectable()
export class LanguageService {
  constructor(
    @InjectRepository(Language) private langRepository: Repository<Language>,
  ) {}

  async getLanguage(langKey: string, params?: unknown): Promise<string> {
    try {
      //TODO: get langId from current user
      const result = await this.langRepository.findOne({
        langKey,
        langId: LANGUAGE_ID.JAPANESE,
      });
      if (!result) return langKey;
      return parseLanguageWithParams(result.langValue, params);
    } catch (error) {
      //Handle exception here
      return langKey;
    }
  }
}
