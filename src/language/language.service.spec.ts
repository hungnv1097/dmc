import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Language } from './entity/language.entity';
import { LanguageService } from './language.service';

describe('LanguageService', () => {
  let service: LanguageService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'root',
          database: 'dmc_cloud',
          entities: [Language],
        }),
        TypeOrmModule.forFeature([Language]),
      ],
      providers: [LanguageService],
    }).compile();

    service = module.get<LanguageService>(LanguageService);
  });

  afterAll(async () => {
    module.close();
  });

  it('should be defined', async () => {
    expect(service).toBeDefined();
  });

  it('get single key language', () => {
    return expect(service.getLanguage('login_invalid')).resolves.toEqual(
      '{login_id} が未登録',
    );
  });

  it('get single key language with params', () => {
    return expect(
      service.getLanguage('login_invalid', { login_id: '0002' }),
    ).resolves.toEqual('0002 が未登録');
  });

  it('get language with not found key', () => {
    return expect(
      service.getLanguage('not_found_language_key'),
    ).resolves.toEqual('not_found_language_key');
  });
});
