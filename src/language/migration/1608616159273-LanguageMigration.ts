import { MigrationInterface, QueryRunner } from 'typeorm';

export class LanguageMigration1608616159273 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      '\
            CREATE TABLE `cnf_language` (\
                `id` int NOT NULL AUTO_INCREMENT,\
                `lang_id` varchar(10) NOT NULL,\
                `lang_key` varchar(255) NOT NULL,\
                `lang_value` varchar(255) NOT NULL,\
                `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                `deleted_at` datetime(6) NULL,\
                PRIMARY KEY (`id`),\
                INDEX `lang_id_lang_key` (`lang_id` ASC, `lang_key` ASC)\
            ) ENGINE = InnoDB\
        ',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('\
            DROP TABLE `cnf_language`\
        ');
  }
}
