import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'cnf_language',
})
export class Language {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'lang_id',
  })
  langId: string;

  @Column({
    name: 'lang_key',
  })
  langKey: string;

  @Column({
    name: 'lang_value',
  })
  langValue: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
  })
  deletedAt: Date;
}
