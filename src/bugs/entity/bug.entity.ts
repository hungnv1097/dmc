import { Photo } from 'src/photo/entity/photo.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'tbl_report',
})
export class Bug {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    name: 'create_user_id',
    length: 45,
  })
  createUserId: string;

  @Column({
    name: 'update_user_id',
    length: 45,
  })
  updateUserId: string;

  @Column({
    name: 'report_date',
  })
  reportDate: Date;

  @Column({
    name: 'category1',
    length: 100,
    nullable: true,
  })
  category1: string;

  @Column({
    name: 'category2',
    length: 100,
    nullable: true,
  })
  category2: string;

  @Column({
    name: 'category3',
    length: 100,
    nullable: true,
  })
  category3: string;

  @Column({
    name: 'shot_cnt',
    nullable: true,
  })
  shotCnt: number;

  @Column({
    name: 'launch_place',
    length: 45,
    nullable: true,
  })
  launchPlace: string;

  @Column({
    name: 'screen_id',
    length: 45,
    nullable: true,
  })
  screenId: string;

  @Column({
    name: 'report_status',
    length: 45,
    nullable: true,
  })
  reportStatus: string;

  @Column({
    name: 'complete_date',
  })
  completeDate: Date;

  @Column({
    name: 'dead_line',
  })
  deadLine: Date;

  @Column({
    name: 'check_user',
    length: 45,
    nullable: true,
  })
  checkUser: string;

  @Column({
    name: 'worker',
    length: 45,
    nullable: true,
  })
  worker: string;

  @OneToMany(
    () => Photo,
    photo => photo.bug,
  )
  photos: Photo[];

  @Column({
    name: 'mold_no',
    length: 45,
    nullable: true,
  })
  moldNo: string;

  @Column({
    name: 'product_code',
    length: 45,
    nullable: true,
  })
  productCode: string;

  @Column({
    name: 'content_code',
    length: 45,
  })
  contentCode: string;

  @CreateDateColumn({
    name: 'create_date',
  })
  createDate: Date;

  @UpdateDateColumn({
    name: 'update_date',
  })
  updateDate: Date;
}
