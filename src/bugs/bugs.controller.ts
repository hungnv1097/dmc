import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { PaginateResult } from 'src/shared/interface/paginate-result.interface';
import { BugsService } from './bugs.service';
import { CreateBugDto } from './dto/create-bug.dto';
import { FilterBugDto } from './dto/filterbug.dto';
import { UpdateBugDto } from './dto/update-bug.dto';
import { Bug } from './entity/bug.entity';

@Controller('bugs')
export class BugsController {
  constructor(private readonly bugsService: BugsService) {}

  @Post()
  async create(@Body() createBugDto: CreateBugDto): Promise<Bug> {
    return this.bugsService.create(createBugDto);
  }

  @Get()
  findAll(@Query() query: FilterBugDto): Promise<PaginateResult<Bug>> {
    return this.bugsService.findAll(query);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Bug> {
    return this.bugsService.findOne(id);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBugDto: UpdateBugDto,
  ): Promise<Bug> {
    return this.bugsService.update(id, updateBugDto);
  }

  @Delete()
  remove(@Query() deleteDto) {
    return this.bugsService.remove(deleteDto);
  }
}
