import { MigrationInterface, QueryRunner } from 'typeorm';

export class BugMigration1614838885828 implements MigrationInterface {
  name = 'BugMigration1614838885828';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      '\
                CREATE TABLE `tbl_report` (\
                    `id` varchar(45) NOT NULL,\
                    `create_user_id` varchar(45) NOT NULL,\
                    `update_user_id` varchar(45) NOT NULL,\
                    `report_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                    `remarks` varchar(1000),\
                    `del_flg` char(1) NOT NULL DEFAULT "0",\
                    `report_category` varchar(100) NOT NULL,\
                    `part_id` varchar(45) NOT NULL,\
                    `product_code` varchar(45),\
                    `seq` int(45),\
                    `mold_no` varchar(45),\
                    `company_name` varchar(45),\
                    `product_no` varchar(100),\
                    `order_no` varchar(45),\
                    `target_part` varchar(100),\
                    `category1` varchar(100),\
                    `category2` varchar(100),\
                    `category3` varchar(100),\
                    `shot_cnt` int(11),\
                    `product_name` varchar(500),\
                    `correct_plan` varchar(1000),\
                    `correct_content` varchar(1000),\
                    `launch_place` varchar(45),\
                    `parts_region` varchar(100),\
                    `screen_id` varchar(45),\
                    `report_status` varchar(45),\
                    `complete_date` timestamp,\
                    `project_id` varchar(45),\
                    `check_id` varchar(40),\
                    `editable` char(1),\
                    `dead_line` timestamp,\
                    `check_user` varchar(45),\
                    `priority` varchar(45),\
                    `emd` varchar(45),\
                    `correct_flg` varchar(45),\
                    `proc_path` varchar(45),\
                    `layer3_id` varchar(45),\
                    `worker` varchar(45),\
                    `proc_id` varchar(45),\
                    `layer1_id` varchar(45),\
                    `layer2_id` varchar(45),\
                    `approver` varchar(45),\
                    `approved_date` timestamp,\
                    `dept_id` varchar(45),\
                    `cost_down_effect` varchar(45),\
                    `knowledge_id` varchar(45),\
                    `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                    `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),\
                    `deleted_at` datetime(6) NULL,\
                    PRIMARY KEY (`id`)\
                ) ENGINE = InnoDB\
            ',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      '\
                DROP TABLE `tbl_report`\
            ',
    );
  }
}
