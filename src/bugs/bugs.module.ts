import { Module } from '@nestjs/common';
import { BugsService } from './bugs.service';
import { BugsController } from './bugs.controller';
import { Bug } from './entity/bug.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Photo } from 'src/photo/entity/photo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bug]), TypeOrmModule.forFeature([Photo])],
  controllers: [BugsController],
  providers: [BugsService],
})
export class BugsModule {}
