import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, Length } from 'class-validator';
import { CreatePhotoDto } from 'src/photo/dto/create-photo.dto';

export class CreateBugDto {
  @IsOptional()
  @ApiProperty()
  readonly reportDate: Date;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty()
  @Length(45)
  readonly createUserId: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty()
  @Length(45)
  readonly updateUserId: string;

  @IsOptional()
  @ApiProperty()
  readonly category1?: string;

  @IsOptional()
  @ApiProperty()
  readonly category2?: string;

  @IsOptional()
  @ApiProperty()
  readonly category3?: string;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  readonly shotCnt?: number;

  @IsOptional()
  @ApiProperty()
  readonly reportStatus: string;

  @IsOptional()
  @ApiProperty()
  readonly completeDate: Date;

  @IsOptional()
  @ApiProperty()
  readonly deadLine: Date;

  @IsOptional()
  @ApiProperty()
  readonly launchPlace: string;

  @IsOptional()
  @ApiProperty()
  readonly moldNo: string;

  @IsOptional()
  @ApiProperty()
  readonly worker?: string;

  @IsOptional()
  @ApiProperty()
  readonly screenId: string;

  @IsOptional()
  @ApiProperty()
  readonly checkUser: string;

  @IsOptional()
  @ApiProperty()
  readonly photos: CreatePhotoDto[];

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty()
  readonly contentCode: string;
}
