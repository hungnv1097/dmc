import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional } from 'class-validator';
import { SORT_TYPE } from 'src/shared/constants/pagination.constant';

export class FilterBugDto {
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  @ApiProperty()
  readonly page: string;

  @IsNumber()
  @Type(() => Number)
  @ApiProperty()
  @IsOptional()
  limit: string;

  @IsOptional()
  @ApiPropertyOptional()
  sortField: string;

  @IsOptional()
  @Transform(sortOrder => sortOrder.toUpperCase())
  @IsEnum(SORT_TYPE)
  @ApiPropertyOptional()
  sortOrder: SORT_TYPE;

  @IsOptional()
  @ApiPropertyOptional()
  productCode: string;

  // @IsOptional()
  // @ApiPropertyOptional()
  // contentCode: string;

  @IsOptional()
  @ApiPropertyOptional()
  reportStatus: string;

  @IsOptional()
  @ApiPropertyOptional()
  moldNo: string;

  @IsOptional()
  @ApiPropertyOptional()
  startDate: Date;

  @IsOptional()
  @ApiPropertyOptional()
  endDate: Date;
}
