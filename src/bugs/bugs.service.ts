import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Photo } from 'src/photo/entity/photo.entity';
import { Message } from 'src/shared/constants/system.constant';
import { Between, Like, Repository } from 'typeorm';
import { CreateBugDto } from './dto/create-bug.dto';
import { UpdateBugDto } from './dto/update-bug.dto';
import { Bug } from './entity/bug.entity';
import {
  PAGINATE_DEFAULT,
  SORT_TYPE,
} from 'src/shared/constants/pagination.constant';
import { FilterBugDto } from './dto/filterbug.dto';
import { isEmpty } from 'class-validator';
import { UpdatePhotoDto } from 'src/photo/dto/update-photo.dto';
import { DEFAULT_DATE, FIELDS_QUERY } from 'src/shared/constants/bug.constant';
import { PaginateResult } from 'src/shared/interface/paginate-result.interface';

@Injectable()
export class BugsService {
  constructor(
    @InjectRepository(Bug) private bugRepository: Repository<Bug>,
    @InjectRepository(Photo) private photoRepository: Repository<Photo>,
  ) {}
  async create(createBugDto: CreateBugDto) {
    const bug = await this.bugRepository.save({
      ...createBugDto,
      updateUserId: createBugDto.createUserId,
    });

    if (!bug) this.throwBugCreateFail();

    let photos = [];
    if (createBugDto.photos) {
      const photoCreate = createBugDto.photos.map(photo => ({
        ...photo,
        createUserId: createBugDto.createUserId,
        updateUserId: createBugDto.updateUserId,
        reportId: bug.id,
      }));

      photos = await this.photoRepository.save(photoCreate);

      if (!photos) {
        this.throwBugCreateFail();
      }
    }

    return {
      ...bug,
      photos,
    };
  }

  async findAll(query: FilterBugDto): Promise<PaginateResult<Bug>> {
    const { sortField, sortOrder, startDate, endDate } = query;

    const limit = parseInt(query.limit) || PAGINATE_DEFAULT.LIMIT_LAUNCH;
    const page = parseInt(query.page) || 1;
    const skip = page * limit - limit;

    const options = {
      order: {},
      take: limit,
      skip,
      where: {},
    };

    if (!isEmpty(sortField)) {
      options.order[sortField] = sortOrder || SORT_TYPE.ASC;
    }

    let start = new Date(DEFAULT_DATE.start);
    let end = new Date(DEFAULT_DATE.end);
    if (!isEmpty(startDate)) {
      start = startDate;
    }
    if (!isEmpty(endDate)) {
      end = endDate;
    }

    options.where['reportDate'] = Between(start, end);

    for (const [key, value] of Object.entries(query)) {
      if (FIELDS_QUERY.includes(key) || [null, ''].includes(value)) continue;
      options.where[key] = Like(`%${value}%`);
    }

    const [bugLst, total] = await this.bugRepository.findAndCount(options);

    return {
      data: bugLst,
      total,
      page,
    };
  }

  async findOne(id: string): Promise<Bug> {
    const bug = await this.bugRepository.findOne(id, { relations: ['photos'] });
    if (!bug) this.throwBugNotFound();

    return bug;
  }

  async update(id: string, updateBugDto: UpdateBugDto): Promise<Bug> {
    const bug = await this.bugRepository.findOne(id);
    if (!bug) this.throwBugNotFound();

    let photos = [];
    if (updateBugDto.photos) {
      const photoUpdate: UpdatePhotoDto[] = updateBugDto.photos.map(photo => ({
        ...photo,
        updateUserId: updateBugDto.updateUserId,
      }));

      photos = await this.photoRepository.save(photoUpdate);

      if (!photos) {
        this.throwBugCreateFail();
      }
    }

    return this.bugRepository.save({ ...bug, ...updateBugDto });
  }

  remove(deleteDto: any) {
    return this.bugRepository.delete(deleteDto.id);
  }

  /** throw error */
  throwBugNotFound(): never {
    throw new HttpException(
      {
        message: Message.bugNotFound,
        statusCode: 404,
      },
      HttpStatus.BAD_REQUEST,
    );
  }

  throwBugUpdateFail(): never {
    throw new HttpException(
      {
        message: Message.updateFailed,
        statusCode: 500,
      },
      HttpStatus.BAD_REQUEST,
    );
  }

  throwBugCreateFail(): never {
    throw new HttpException(
      {
        message: Message.createFailed,
        statusCode: 500,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}
