import { User } from '../../users/entity/user.entity';
import { UpdateUserDTO } from '../../users/dto/update-user.dto';

export function convertDtoToUser(
  dto: UpdateUserDTO,
  exitedUser: User = null,
): User {
  let user: User;
  if (exitedUser) {
    user = exitedUser;
  } else {
    user = new User();
  }

  user.username = dto.username;
  user.userId = dto.userId;
  return user;
}
