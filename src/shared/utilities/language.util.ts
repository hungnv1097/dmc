export const parseLanguageWithParams = (
  langValue: string,
  params?: any,
): string => {
  const regex = /{(.+?)}/gm;
  let resultText = langValue;
  let m;

  while ((m = regex.exec(resultText)) !== null) {
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }
    const [resultKey, paramKey] = m;
    if (params && Object.prototype.hasOwnProperty.call(params, paramKey)) {
      resultText = resultText.replace(resultKey, params[paramKey]);
    }
  }
  return resultText;
};
