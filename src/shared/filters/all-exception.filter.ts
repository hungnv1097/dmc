import { ArgumentsHost, Catch, HttpException } from '@nestjs/common';
import { CustomException } from './custom-exception';

@Catch()
export class AllExceptionFilter extends CustomException<HttpException> {
  catch(exception: any, host: ArgumentsHost) {
    const statusCode = exception ? 500 : exception.getStatus();
    const errmsg = exception.response || exception.message;
    const isExists = errmsg && errmsg.statusCode;
    const message = isExists ? errmsg.message : errmsg;
    const errorCode = isExists ? errmsg.statusCode : statusCode;
    const data = {
      statusCode,
      errorCode,
      message,
    };

    this.send(exception, host, data);
  }
}
