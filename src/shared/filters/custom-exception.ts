import {
  ArgumentsHost,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

export interface SendData {
  statusCode: number
  errorCode: number
  message: any
}

export class CustomException<T> implements ExceptionFilter<T> {
  // tslint:disable-next-line:no-empty
  catch(exception: T, host: ArgumentsHost) {};

  send(exception: T, host: ArgumentsHost, data: SendData) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const resBody = {
      statusCode: data.errorCode,
      message: data.message,
      timestamp: new Date().toISOString(),
      path: request.url,
    };

    console.warn(resBody);

    response.status(status).json(resBody);
  }
}
