export const PAGINATE_DEFAULT = {
  LIMIT_LAUNCH: 5,
};

export enum SORT_TYPE {
  ASC = 'ASC',
  DESC = 'DESC',
}
