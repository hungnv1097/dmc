//TODO: replace with requirement
export const LANGUAGE_ID = {
  JAPANESE: '0001',
  ENGLISH: '0002',
  CHINESE: '0003',
};

export const Message = {
  /** Common */
  updateFailed: 'Update fail',
  createFailed: 'Create fail',

  /** Bug */
  bugNotFound: 'Bug not found',
};
