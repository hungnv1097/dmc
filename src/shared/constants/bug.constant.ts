export const DEFAULT_DATE = {
  start: '1/1/1987',
  end: '1/1/2050',
};

export const FIELDS_QUERY = [
  'page',
  'limit',
  'sortField',
  'sortOrder',
  'startDate',
  'endDate',
];
