import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, Length, Max } from 'class-validator';
export class CreatePhotoDto {
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @Length(45)
  @ApiProperty()
  readonly createUserId: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @Length(45)
  @ApiProperty()
  readonly updateUserId: string;

  @IsOptional()
  @Max(500)
  @ApiProperty()
  readonly path?: string;

  @IsOptional()
  @ApiProperty()
  readonly comment: string;

  @IsOptional()
  @IsNotEmpty()
  @Length(1)
  @ApiProperty()
  readonly status: string;

  @IsOptional()
  @ApiProperty()
  readonly delFlg: string;
}
