import { MigrationInterface, QueryRunner } from 'typeorm';

export class PhotoMigration1615101321699 implements MigrationInterface {
  name = 'PhotoMigration1615101321699';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      '\
                CREATE TABLE `tbl_photo` (\
                    `id` varchar(36) NOT NULL,\
                    `create_user_id` varchar(45) NOT NULL,\
                    `update_user_id` varchar(45) NOT NULL,\
                    `report_id` varchar(45) NOT NULL, \
                    `path` varchar(500) NOT NULL, \
                    `comment` varchar(1000) NULL, \
                    `status` varchar(1) NOT NULL, \
                    `del_flg` varchar(1) NOT NULL DEFAULT "0",\
                    `create_date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                    `update_date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),\
                    PRIMARY KEY (`id`)\
                ) ENGINE = InnoDB\
            ',
    );
    await queryRunner.query(
      'ALTER TABLE `tbl_photo`\
            ADD CONSTRAINT `FK_cf404af9db590de1d4f7d1884f3` FOREIGN KEY (`report_id`)\
            REFERENCES `tbl_report`(`id`)\
            ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `tbl_photo` DROP FOREIGN KEY `FK_cf404af9db590de1d4f7d1884f3`',
    );
    await queryRunner.query('DROP TABLE `tbl_photo`');
  }
}
