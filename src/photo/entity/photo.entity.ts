import { Bug } from 'src/bugs/entity/bug.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'tbl_photo',
})
export class Photo {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    name: 'create_user_id',
    length: 45,
  })
  createUserId: string;

  @Column({
    name: 'update_user_id',
    length: 45,
  })
  updateUserId: string;

  @Column({
    name: 'report_id',
    length: 45,
  })
  reportId: string;

  @Column({
    name: 'path',
    length: 500,
  })
  path: string;

  @Column({
    name: 'comment',
    length: 1000,
    nullable: true,
  })
  comment: string;

  @Column({
    name: 'status',
    length: 1,
  })
  status: string;

  @ManyToOne(
    () => Bug,
    bug => bug.photos,
  )
  @JoinColumn({ name: 'report_id' })
  bug: Bug;

  @Column({
    name: 'del_flg',
    length: 1,
    default: '0',
  })
  delFlg: string;

  @CreateDateColumn({
    name: 'create_date',
  })
  createDate: Date;

  @UpdateDateColumn({
    name: 'update_date',
  })
  updateDate: Date;
}
