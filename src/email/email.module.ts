import { Global, Module, Provider } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import { SMTP_EMAIL_CONFIG } from './config/email.config';
import { EMAIL_TRANSPORT_TOKEN } from './constant/inject-token.constant';
import { EmailService } from './email.service';

const EmailProvider: Provider = {
  provide: EMAIL_TRANSPORT_TOKEN,
  useFactory: () => {
    return createTransport({ ...SMTP_EMAIL_CONFIG.smtp });
  },
};

@Global()
@Module({
  providers: [EmailService, EmailProvider],
  exports: [EmailService, EMAIL_TRANSPORT_TOKEN],
})
export class EmailModule {}
