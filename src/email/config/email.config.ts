type EmailConfig = {
  from?: string;
  smtp: {
    host: string;
    port: number;
    auth?: {
      user: string;
      pass: string;
    };
  };
};

const EMAIL_CONFIG = {
  development: {
    from: 'f9a5320dfc-e757ee@inbox.mailtrap.io',
    smtp: {
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: 'c802cee1f4f7f2',
        pass: 'f9a7f104f8eb69',
      },
    },
  },
  production: {
    from: process.env.EMAIL_FROM,
    smtp: {
      host: process.env.EMAIL_HOST,
      port: Number(process.env.EMAIL_PORT) || 25,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    },
  },
};

export const SMTP_EMAIL_CONFIG: EmailConfig =
  EMAIL_CONFIG[process.env.NODE_ENV || 'development'];
