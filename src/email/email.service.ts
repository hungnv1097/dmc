import { Inject, Injectable } from '@nestjs/common';
import { EMAIL_TRANSPORT_TOKEN } from './constant/inject-token.constant';
import { Transporter } from 'nodemailer';
import { EmailDTO } from './dto/email.dto';
import { compile, Template } from 'handlebars';
import { SMTP_EMAIL_CONFIG } from './config/email.config';
import { join } from 'path';
import { readFileSync } from 'fs';

@Injectable()
export class EmailService {
  constructor(
    @Inject(EMAIL_TRANSPORT_TOKEN) private transporter: Transporter,
  ) {}

  public async sendEmail(emailDTO: EmailDTO): Promise<boolean> {
    const { from, to, cc = '', bcc = '', subject, content } = emailDTO;
    const sendFrom = from || SMTP_EMAIL_CONFIG.from;
    const sendTo = Array.isArray(to) ? to.join(',') : to;
    await this.transporter.sendMail({
      from: sendFrom,
      to: sendTo,
      subject,
      cc,
      bcc,
      text: content,
      encoding: 'UTF-8',
    });
    return true;
  }

  public async sendEmailTemplate(
    emailDTO: EmailDTO,
    templateUrl: string,
  ): Promise<boolean> {
    const { from, to, cc = '', bcc = '', subject, context } = emailDTO;
    const sendFrom = from || SMTP_EMAIL_CONFIG.from;
    const sendTo = Array.isArray(to) ? to.join(',') : to;

    const emailTemplatePath = join(__dirname, '.', 'template', templateUrl);
    const templateSource = readFileSync(emailTemplatePath, 'utf8');
    const template: Template = compile(templateSource);
    const parsedHtml = template(context);

    await this.transporter.sendMail({
      from: sendFrom,
      to: sendTo,
      subject,
      cc,
      bcc,
      html: parsedHtml,
      encoding: 'UTF-8',
    });
    return true;
  }
}
