import { Injectable } from '@nestjs/common';
import { LanguageService } from './language/language.service';

@Injectable()
export class AppService {
  constructor(private langService: LanguageService) {}

  getHello(): any {
    //Example using with params
    return this.langService.getLanguage('login_invalid', { login_id: '0002' });
  }
}
