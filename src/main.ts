import * as dotenv from 'dotenv';
dotenv.config();
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import helmet from 'helmet';
import { ValidationPipe } from '@nestjs/common';
import { nestCsrf } from 'ncsrf';
import cookieParser from 'cookie-parser';
import { AllExceptionFilter } from './shared/filters/all-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  const options = new DocumentBuilder()
    .setTitle('DMC Cloud API')
    .setDescription('The DMC Cloud API description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  //Require use cookieParser for nestCsrf
  app.use(cookieParser());
  app.use(nestCsrf({ ttl: 86400 }));
  //Use for custom response message
  app.useGlobalFilters(new AllExceptionFilter());
  //Need replace origin to web client url

  app.enableCors({ origin: ['http://localhost:4200'], credentials: true });
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
