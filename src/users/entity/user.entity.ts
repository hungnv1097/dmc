import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

export type LanguageType = 'en-US' | 'jp-JP' | 'cn-CN';

export enum AuthorityGroupRole {
  ROOT = 'Root',
  LEVEL1 = 'level 1',
  LEVEL2 = 'level 2',
}

@Entity({
  name: 'mst_user',
})
@Unique(['userId'])
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 45,
  })
  username: string;

  @Column({
    name: 'password',
  })
  password: string;

  @Column({
    name: 'user_id',
  })
  userId: string;

  @Column({
    name: 'mail_address',
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  mailAddress: string;

  @Column({
    type: 'enum',
    enum: ['en-US', 'jp-JP', 'cn-CN'],
    default: 'en-US',
  })
  language: LanguageType;

  @Column({
    name: 'timezone',
    nullable: true,
  })
  timezone: string;

  @Column({
    name: 'authority_group',
    type: 'enum',
    enum: AuthorityGroupRole,
    default: AuthorityGroupRole.LEVEL2,
  })
  authorityGroup: AuthorityGroupRole;

  @Column({
    name: 'company_name',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  companyName: string;

  @Column({
    name: 'del_flg',
    type: 'boolean',
    default: true,
  })
  delFlg: boolean;

  @Column({
    name: 'create_user_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  createUserId: string;

  @Column({
    name: 'update_user_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  updateUserId: string;

  @Column({
    name: 'remarks',
    type: 'varchar',
    length: 500,
    nullable: true,
  })
  remarks: string;

  @Column({
    name: 'login_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  loginId: string;

  @Column({
    name: 'user_type',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  userType: string;

  @Column({
    name: 'editable',
    type: 'boolean',
    default: true,
  })
  editTable: boolean;

  @Column({
    name: 'auth_id',
    type: 'varchar',
    default: 45,
    nullable: true,
  })
  authId: string;

  @Column({
    name: 'auth_name',
    type: 'varchar',
    default: 45,
    nullable: true,
  })
  authName: string;

  @Column({
    name: 'group_name',
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  groupName: string;

  @Column({
    name: 'group_name_2',
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  groupName2: string;

  @Column({
    name: 'locale_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  localeId: string;

  @Column({
    name: 'supplier_flg',
    type: 'boolean',
    default: false,
  })
  supplierFlg: boolean;

  @Column({
    name: 'development_base_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  developmentBaseId: string;

  @Column({
    name: 'hashed_password',
    type: 'varchar',
    length: 256,
    nullable: true,
  })
  hashedPassword: string;

  @Column({
    name: 'password_changed_at',
    type: 'date',
    nullable: true,
  })
  passwordChangedAt: Date;

  @Column({
    name: 'password_expires_at',
    type: 'date',
    nullable: true,
  })
  passwordExpiresAt: Date;

  @Column({
    name: 'valid_flg',
    type: 'boolean',
    default: true,
  })
  validFlg: boolean;

  @Column({
    name: 'initial_password_flg',
    type: 'boolean',
    default: true,
  })
  initialPasswordFlg: boolean;

  @Column({
    name: 'login_fail_count',
    nullable: true,
  })
  loginFailCount: number;

  @Column({
    name: 'indefinite_flg',
    type: 'boolean',
    default: false,
  })
  indefiniteFlg: boolean;

  @Column({
    name: 'account_locked',
    type: 'boolean',
    default: false,
  })
  accountLocked: boolean;

  @Column({
    name: 'invited_user_id',
    type: 'varchar',
    length: 45,
    nullable: true,
  })
  invitedUserId: string;

  @Column({
    name: 'company_id',
    type: 'varchar',
    length: 45,
    default: '9999',
  })
  companyId: string;

  @Column({
    name: 'reset_password_token',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  resetPasswordToken: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
  })
  deletedAt: Date;
}
