import * as crypto from 'crypto';
import {
  Injectable,
  BadRequestException,
  NotFoundException,
  HttpStatus,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { contains } from 'class-validator';
import { Repository } from 'typeorm';
import { CreateUserDTO } from './dto/create-user.dto';
import { FilterUserDto } from './dto/filter-user.dto';
import { UpdateUserDTO } from './dto/update-user.dto';
import { User } from './entity/user.entity';
import { UserResult } from './interface/user-result.interface';
import { convertDtoToUser } from '../shared/utilities/convert-dto.utilities';
import { ResetPasswordDTO } from './dto/reset-password.dto';
import { EmailService } from 'src/email/email.service';
import { RESET_PASSWORD_TEMPLATE } from 'src/email/constant/template-url.constant';
import { ChangePasswordDTO } from './dto/change-password.dto';
/* eslint-disable */
const bcrypt = require('bcrypt');

@Injectable()
export class UserService {
  private readonly users: User[];

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    private readonly emailService: EmailService,
  ) {}

  async find(filter: FilterUserDto): Promise<UserResult> {
    const userBuiler = this.userRepository.createQueryBuilder('user');
    userBuiler.select([
      'user.id',
      'user.username',
      'user.userId',
      'user.validFlg',
      'user.mailAddress',
      'user.language',
      'user.timezone',
      'user.authorityGroup',
      'user.companyName',
      'user.createdAt',
    ]);
    if (filter) {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
    }
    if (filter) {
      if (filter.userId) {
        userBuiler.andWhere(`user.userId like :userId`, {
          userId: `%${filter.userId}%`,
        });
      }
      if (filter.username) {
        userBuiler.andWhere(`user.username like :username`, {
          username: `%${filter.username}%`,
        });
      }
      if (typeof filter.validFlag === 'string') {
        userBuiler.andWhere(`user.validFlg = :validFlg`, {
          validFlg: filter.validFlag == 'true',
        });
      }
    }
    filter.sortField &&
      filter.sortType &&
      userBuiler.addOrderBy(filter.sortField, filter.sortType, 'NULLS LAST');
    filter.limit && userBuiler.take(filter.limit);
    filter.skip ? userBuiler.skip(filter.skip) : userBuiler.skip(0);

    const result = await userBuiler.getManyAndCount();
    return {
      data: result[0],
      total: result[1],
    };
  }

  async findById(id: string): Promise<User> {
    const existedUserId = await this.userRepository.findOne({
      id: id,
    });
    if (!existedUserId) {
      throw new BadRequestException('User not found');
    }
    return existedUserId;
  }

  async findByUsername(username: string): Promise<User> {
    const existedUsername = await this.userRepository.findOne({
      username: username,
    });
    if (!existedUsername) {
      throw new BadRequestException('User not found');
    }
    return existedUsername;
  }

  async create(createUserDto: CreateUserDTO): Promise<User> {
    const existedUserId = await this.userRepository.findOne({
      userId: createUserDto.userId,
    });
    if (existedUserId) {
      throw new BadRequestException('user_id registed');
    }
    const user: User = convertDtoToUser(createUserDto);
    if (
      contains(createUserDto.password, ' ') &&
      createUserDto.password.includes(' ')
    ) {
      throw new BadRequestException('Password has space');
    }
    user.password = await this.hashPassword(createUserDto.password);
    return this.userRepository.save(user);
  }

  async update(id: string, updateUserDto: UpdateUserDTO): Promise<User> {
    let user = await this.userRepository.findOne(id);
    if (!user) {
      throw new BadRequestException('User not found');
    }
    user = convertDtoToUser(updateUserDto, user);

    return this.userRepository.save(user);
  }

  async delete(id: string) {
    this.userRepository.softDelete(id);

    return this.userRepository.findOne(id, {
      withDeleted: true,
    });
  }

  async findByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({
      userId: email,
    });
  }

  async verifyResetPasswordToken(resetPasswordToken: string): Promise<any> {
    const existedUser = await this.userRepository.findOne({
      resetPasswordToken,
    });
    if (!existedUser) {
      throw new NotFoundException('Token not found');
    }
    return {
      email: existedUser.mailAddress,
    };
  }

  async resetPassword({ email, url }: ResetPasswordDTO) {
    const existedUser = await this.findByEmail(email);
    if (!existedUser) {
      throw new NotFoundException('User not found');
    }

    const resetPasswordToken = crypto
      .createHash('sha256')
      .update(Date.now().toString())
      .digest('hex');
    existedUser.resetPasswordToken = resetPasswordToken;
    const returnUrl = url + resetPasswordToken;
    await this.emailService.sendEmailTemplate(
      {
        to: email,
        subject: 'DMC-CLOUD',
        context: {
          title: 'DMC-CLOUD',
          reset_url: returnUrl,
        },
      },
      RESET_PASSWORD_TEMPLATE,
    );
    await this.userRepository.save(existedUser);
  }

  async changePasswordWithToken(
    resetPasswordToken: string,
    { password, confirmPassword }: ChangePasswordDTO,
  ) {
    const existedUser = await this.userRepository.findOne({
      resetPasswordToken,
    });
    if (!existedUser) {
      throw new NotFoundException('Token not found');
    }
    if (confirmPassword != password) {
      throw new BadRequestException('Confirm password incorrect');
    }
    existedUser.password = await this.hashPassword(password);
    existedUser.resetPasswordToken = null;
    await this.userRepository.save(existedUser);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success',
    };
  }

  async hashPassword(plainPassword: string) {
    return bcrypt.hash(plainPassword, 10);
  }

  async compareHashedPassword(plain: string, hashed: string) {
    return bcrypt.compare(plain, hashed);
  }
}
