import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class FilterUserDto {
  @ApiPropertyOptional()
  @IsOptional()
  userId?: string;

  @ApiPropertyOptional()
  @IsOptional()
  username?: string;

  @ApiPropertyOptional()
  @IsOptional()
  validFlag?: string;

  @ApiPropertyOptional()
  @IsOptional()
  limit: number;

  @ApiPropertyOptional()
  @IsOptional()
  skip: number;

  @ApiPropertyOptional()
  @IsOptional()
  sortField?: string;

  @ApiPropertyOptional()
  @IsOptional()
  sortType?: 'DESC' | 'ASC';
}
