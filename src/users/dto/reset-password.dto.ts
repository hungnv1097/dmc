import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ResetPasswordDTO {
  @ApiProperty()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  // @IsNotEmpty()
  url?: string;
}
