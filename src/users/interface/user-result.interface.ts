import { User } from '../entity/user.entity';

export interface UserResult {
  data: Array<User>;
  total: number;
}
