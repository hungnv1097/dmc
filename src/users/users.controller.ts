import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ChangePasswordDTO } from './dto/change-password.dto';
import { CreateUserDTO } from './dto/create-user.dto';
import { FilterUserDto } from './dto/filter-user.dto';
import { ResetPasswordDTO } from './dto/reset-password.dto';
import { UpdateUserDTO } from './dto/update-user.dto';
import { UserResult } from './interface/user-result.interface';
import { User } from './interface/user.interface';
import { UserService } from './user.service';

@ApiTags('users')
@Controller('users')
// TODO: un-comment below line to enable authentication for user controller
@UseGuards(new JwtAuthGuard())
@ApiBearerAuth()
export class UsersController {
  constructor(private userService: UserService) {}
  @Post()
  async create(@Body() createUserDTO: CreateUserDTO): Promise<User> {
    return this.userService.create(createUserDTO);
  }

  @Get()
  async find(@Query() filterUserDto: FilterUserDto): Promise<UserResult> {
    return this.userService.find(filterUserDto);
  }

  @Get(':id')
  @ApiParam({ name: 'id' })
  async findById(@Param('id') id: string): Promise<User> {
    return this.userService.findById(id);
  }

  @Put(':id')
  @ApiParam({ name: 'id' })
  async update(
    @Param('id') id: string,
    @Body() updateUserDTO: UpdateUserDTO,
  ): Promise<User> {
    return this.userService.update(id, updateUserDTO);
  }

  @Delete(':id')
  @ApiParam({ name: 'id' })
  async delete(@Param('id') id: string): Promise<User> {
    return this.userService.delete(id);
  }

  @Get('/reset-password/:token')
  async verifyResetPasswordToken(@Param('token') token: string): Promise<any> {
    return this.userService.verifyResetPasswordToken(token);
  }

  @Post('/reset-password/:token')
  async changePasswordWithToken(
    @Param('token') token: string,
    @Body() changePasswordDTO: ChangePasswordDTO,
  ): Promise<any> {
    return this.userService.changePasswordWithToken(token, changePasswordDTO);
  }

  @Post('/reset-password')
  async resetPassword(
    @Body() resetPasswordDTO: ResetPasswordDTO,
  ): Promise<any> {
    return this.userService.resetPassword(resetPasswordDTO);
  }
}
