import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMigration1614656627957 implements MigrationInterface {
  name = 'UserMigration1614656627957';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE users RENAME mst_user');
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `user_id` varchar(255) NOT NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD UNIQUE INDEX `IDX_0afd8559aaf01baa6715a4126a` (`user_id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `mail_address` varchar(100) NULL',
    );
    await queryRunner.query(
      "ALTER TABLE `mst_user` ADD `language` enum ('en-US', 'jp-JP', 'cn-CN') NOT NULL DEFAULT 'en-US'",
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `timezone` varchar(255) NULL',
    );
    await queryRunner.query(
      "ALTER TABLE `mst_user` ADD `authority_group` enum ('Root', 'level 1', 'level 2') NOT NULL DEFAULT 'level 2'",
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `company_name` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `del_flg` tinyint NOT NULL DEFAULT 1',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `create_user_id` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `update_user_id` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `remarks` varchar(500) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `login_id` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `user_type` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `editable` tinyint NOT NULL DEFAULT 1',
    );
    await queryRunner.query(
      "ALTER TABLE `mst_user` ADD `auth_id` varchar(255) NULL DEFAULT '45'",
    );
    await queryRunner.query(
      "ALTER TABLE `mst_user` ADD `auth_name` varchar(255) NULL DEFAULT '45'",
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `group_name` varchar(100) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `group_name_2` varchar(100) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `locale_id` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `supplier_flg` tinyint NOT NULL DEFAULT 0',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `development_base_id` varchar(45) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `hashed_password` varchar(256) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `password_changed_at` date NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `password_expires_at` date NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `valid_flg` tinyint NOT NULL DEFAULT 1',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `initial_password_flg` tinyint NOT NULL DEFAULT 1',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `login_fail_count` int NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `indefinite_flg` tinyint NOT NULL DEFAULT 0',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `account_locked` tinyint NOT NULL DEFAULT 0',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `invited_user_id` varchar(45) NULL',
    );
    await queryRunner.query(
      "ALTER TABLE `mst_user` ADD `company_id` varchar(45) NOT NULL DEFAULT '9999'",
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` CHANGE `id` `id` int NOT NULL',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP PRIMARY KEY');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `id` varchar(36) NOT NULL PRIMARY KEY',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `username`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `username` varchar(45) NOT NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `reset_password_token` varchar(255) NULL',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE mst_user RENAME users');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `username`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `username` varchar(255) NOT NULL',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` ADD `id` int NOT NULL AUTO_INCREMENT',
    );
    await queryRunner.query('ALTER TABLE `mst_user` ADD PRIMARY KEY (`id`)');
    await queryRunner.query(
      'ALTER TABLE `mst_user` CHANGE `id` `id` int NOT NULL AUTO_INCREMENT',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `company_id`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `invited_user_id`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `account_locked`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `indefinite_flg`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `login_fail_count`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `initial_password_flg`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `valid_flg`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `password_expires_at`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `password_changed_at`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `hashed_password`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `development_base_id`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `supplier_flg`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `locale_id`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `reset_password_token`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `group_name_2`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `group_name`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `auth_name`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `auth_id`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `editable`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `user_type`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `login_id`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `remarks`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `update_user_id`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `create_user_id`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `del_flg`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `company_name`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `authority_group`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `timezone`');
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `language`');
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP COLUMN `mail_address`',
    );
    await queryRunner.query(
      'ALTER TABLE `mst_user` DROP INDEX `IDX_0afd8559aaf01baa6715a4126a`',
    );
    await queryRunner.query('ALTER TABLE `mst_user` DROP COLUMN `user_id`');
  }
}
