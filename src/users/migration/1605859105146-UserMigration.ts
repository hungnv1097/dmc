import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMigration1605859105146 implements MigrationInterface {
  name = 'UserMigration1605859105146';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      '\
            CREATE TABLE `users` (\
                `id` int NOT NULL AUTO_INCREMENT,\
                `username` varchar(255) NOT NULL,\
                `password` varchar(255) NOT NULL,\
                `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\
                `deleted_at` datetime(6) NULL,\
                PRIMARY KEY (`id`)\
            ) ENGINE = InnoDB\
        ',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('\
            DROP TABLE `user`\
        ');
  }
}
